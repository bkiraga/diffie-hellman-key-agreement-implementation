import java.security.*;
import java.io.FileInputStream;
import java.io.*;
import java.math.BigInteger;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Assignment1{

    // right to left modular exponentiation based on lecture notes
    private static BigInteger modularExponentiation(BigInteger a, BigInteger b, BigInteger p){
        BigInteger y = new BigInteger("1");
        String bbits = b.toString(2);
        for(int i = 0; i < bbits.length(); i++){
            if(b.testBit(i)){
                y = y.multiply(a).mod(p);
            }
            a = a.multiply(a).mod(p);
        }   
        return y;
    }

    // hashing
    private static byte[] hashKey(byte[] key) throws NoSuchAlgorithmException{
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        return messageDigest.digest(key);
    }

    // aes encryption and padding
    private static byte[] aesEncrypt(byte[] key, byte[] initVector, byte[] input) throws Exception{
        IvParameterSpec iv = new IvParameterSpec(initVector);
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);

        int size = cipher.getBlockSize();
        int padding = size - (input.length % size);
        byte[] paddedInput = new byte[input.length + padding];
        System.arraycopy(input, 0, paddedInput, 0, input.length);

        paddedInput[input.length] = (byte) 128;
        for (int i = 1; i < padding; i++){
            paddedInput[input.length + i] = (byte) 0;
        }

        byte [] encrypted = cipher.doFinal(paddedInput);
        return encrypted;
    }

    // conversion of bigInt to to byte array without the extra leading 0 byte
    private static byte[] bigIntToByte(BigInteger bigInt){
        byte[] result = bigInt.toByteArray();
        if (result[0] == 0){
            byte[] result2 = new byte[result.length - 1];
            System.arraycopy(result, 1, result2, 0, result2.length);
            return result2 ; 
        }
        return result;
    }

    // iv random generator
    private static byte[] generateIV(){
        byte[] iv = new byte[16];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        return iv;
    }

    public static void main(String[] args){

        String primeModulus = "b59dd79568817b4b9f6789822d22594f376e6a9abc0241846de426e5dd8f6eddef00b465f38f509b2b18351064704fe75f012fa346c5e2c442d7c99eac79b2bc8a202c98327b96816cb8042698ed3734643c4c05164e739cb72fba24f6156b6f47a7300ef778c378ea301e1141a6b25d48f1924268c62ee8dd3134745cdf7323";
        String generator = "44ec9d52c8f9189e49cd7c70253c2eb3154dd4f08467a64a0267c9defe4119f2e373388cfa350a4e66e432d638ccdc58eb703e31d4c84e50398f9f91677e88641a2d2f6157e2f4ec538088dcf5940b053c622e53bab0b4e84b1465f5738f549664bd7430961d3e5a2e7bceb62418db747386a58ff267a9939833beefb7a6fd68";
        String geoffsPublicValue = "5af3e806e0fa466dc75de60186760516792b70fdcd72a5b6238e6f6b76ece1f1b38ba4e210f61a2b84ef1b5dc4151e799485b2171fcf318f86d42616b8fd8111d59552e4b5f228ee838d535b4b987f1eaf3e5de3ea0c403a6c38002b49eade15171cb861b367732460e3a9842b532761c16218c4fea51be8ea0248385f6bac0d";
        String mySecretValue = "6B425990F31905EF313D96637B886F26CB6C2C371263B833D84CEBDDD5F47EFF289604DEB1E5B417A3479840388ACE00B56A1FE0C599F45FCBDFD933493ED5D861781AD9F864D9BB25253433F632A296B1C040FB3DC48AD65ED4FC385E6718BFAC9D0DEB98230C9B32D781EC4B50586E100A60255EEF57A2EB41FE67A81C8FD3";

        BigInteger A = new BigInteger(geoffsPublicValue, 16);
        BigInteger b = new BigInteger(mySecretValue, 16);
        BigInteger p = new BigInteger(primeModulus, 16);
        BigInteger g = new BigInteger(generator, 16);

        BigInteger B = modularExponentiation(g, b, p);
        // System.out.println(B.toString(16));

        BigInteger s = modularExponentiation(A, b, p);

        // hardcoded iv value
        String initVector = "3e6555e3eef56cba74a8e8ed93449483";
        BigInteger iv = new BigInteger(initVector,16);
        byte[] ivBytes = bigIntToByte(iv);

        // random generated iv value
        // byte [] ivBytes = generateIV();

        try{
            File inputFile = new File(args[0]);
            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] input = inputStream.readAllBytes();
            inputStream.close();
            byte[] key = bigIntToByte(s);
            byte[] keyDigest = hashKey(key);
            byte [] encryptedBytes = aesEncrypt(keyDigest, ivBytes, input);
            String encryptedHex = new BigInteger(1, encryptedBytes).toString(16);
            System.out.println(encryptedHex);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}